#!/bin/bash

name=$(python3 -c "print('%s.tmp.fna' % '$1'.split('/')[-1][:-7])")
# extract and keep only sequences (temporary approximation to not work with individual sequences)
gunzip $1 -c | grep -v ">" > /bbx/tmp/$name
