#!/bin/bash

for assembly in $(grep -v -e "^#" -e "^@" mapping.tsv | cut -f 5); do size=$(wc -c ${assembly}.tmp.fna | sed 's/ .*//g'); taxid=$(grep -v -e "^#" -e "^@" mapping.tsv | grep -w "${assembly}" | cut -f 1); echo ">${assembly}" | cat - ${assembly}.tmp.fna >> All_seqs.fna; rm ${assembly}.tmp.fna; echo -e "${assembly}\t${size}\t${taxid}" >> len_taxid_file.txt; done




