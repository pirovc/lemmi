#
outp=open('/bbx/output/profile.tsv', 'w')
total=int(open('/bbx/tmp/total.txt').readline().strip())
all_abu={}
ranks={}
ranks_header = '@Ranks:superkingdom|phylum|class|order|family|genus|species\n'#open('/bbx/input/training/mapping.tsv').readlines()[2]
outp.write('# Taxonomic Profiling Output\n')
outp.write('@SampleID:centrifuge\n')
outp.write('@Version:0.9.1\n')
outp.write(ranks_header)
outp.write('@@TAXID\tRANK\tTAXPATH\tTAXPATHSN\tPERCENTAGE\n')
for line in open('bench_report.txt'):
    if line.startswith('name'):
        continue
    id=line.split('\t')[1]
    abu=float(line.split('\t')[5])
    rank=line.split('\t')[2]
    ranks.update({id: rank})
    if id in all_abu:
        all_abu[id] += abu
    else:
        all_abu.update({id: abu})
for i in all_abu:
    outp.write('%s\t%s\t%s\t%s\t%s\n' % (i, ranks[i], i,  i, 100*all_abu[i]/(total/4)))
outp.close()
