from ete3 import NCBITaxa
import gzip

outp = open('/bbx/output/profile.tsv', 'w')

outp.write('# Taxonomic Profiling Output\n@SampleID:CCMetagen\n@Version:1.0\n@Ranks:superkingdom|phylum|class|order|family|genus|species\n@@TAXID\tRANK\tTAXPATH\tTAXPATHSN\tPERCENTAGE\n')

summdepth = 0

mon_dict = {}
ncbi = NCBITaxa()

nucl_tot = 0

# count total reads to normalize the abundance by total reads, not classified
i = 0
with gzip.open('/bbx/input/testing/reads.1.fq.gz', 'rb') as f:
    for line in f:
        i += 1
        if i % 4 == 2:
            nucl_tot += len(line)
i = 0
with gzip.open('/bbx/input/testing/reads.2.fq.gz', 'rb') as f:
    for line in f:
        i += 1
        if i % 4 == 2:
            nucl_tot += len(line)

x=0

for row in open('/bbx/output/results.csv', 'r'):
    if not row.startswith('Closest_match'):
        rank = None
        if row.startswith('"'):
            if row.strip().split('"')[-1].split(',')[18] == "":
                continue
            elif row.strip().split('"')[-1].split(',')[19] != "":
                rank = "species"
            else:
                rank = "genus"

            taxid = row.strip().split('"')[-1].split(',')[11]
            depth = float(row.strip().split('"')[-1].split(',')[8])
            template_lenght = int(row.strip().split('"')[-1].split(',')[3])
            x =+ float(depth * template_lenght)
        else:
            if row.strip().split(',')[18] == "":
                continue
            elif row.strip().split(',')[19] != "":
                rank = "species"
            else:
                rank = "genus"

            taxid = row.strip().split(',')[11]
            depth = float(row.strip().split(',')[8])
            template_lenght = int(row.strip().split(',')[3])
            x =+ float(depth * template_lenght)

        taxidgen = None
        if rank == "species":
            lineage = ncbi.get_lineage(taxid)
            try:
                taxidgen = str([taxid for taxid in lineage if list(ncbi.get_rank([taxid]).values())[0] == 'genus'][0])
            except IndexError:
                pass

        if taxid in mon_dict:
            mon_dict[taxid] += depth
        else:
            mon_dict.update({taxid: depth})

        if taxidgen:
            if taxidgen in mon_dict:
                mon_dict[taxidgen] += depth
            else:
                mon_dict.update({taxidgen: depth})
    else:
        pass

for entry in mon_dict:
    try:
        outp.write("%s\t%s\t%s\t%s\t%s\n" % (entry, list(ncbi.get_rank([entry]).values())[0], entry, entry, mon_dict[entry] * x * 100 / nucl_tot))
    except IndexError:
        print(entry)

outp.close()
