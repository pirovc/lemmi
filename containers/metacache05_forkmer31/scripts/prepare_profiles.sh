taxlevel=$1

# current output profile from metacache outputs names not taxids
# need a mapping names to taxid, eg:
grep -v "#" /bbx/tmp/bench.out | grep -P $"\t${taxlevel}\t" | cut -f 5,7 | sort | uniq > /bbx/tmp/mappings.txt

# keep only the results relative to the taxlevel of interest:
awk '/estimated abundance/,0' /bbx/tmp/abundances.txt | grep $"^${taxlevel}"> /bbx/tmp/abundances_clean.txt
