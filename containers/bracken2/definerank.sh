#!/bin/bash
if [[ $taxlevel == 'species' ]]
then
  rank='S'
elif [[ $taxlevel == 'genus' ]]
then
  rank='G'
elif [[ $taxlevel == 'family' ]]
then
  rank='F'
elif [[ $taxlevel == 'order' ]]
then
  rank='O'
elif [[ $taxlevel == 'class' ]]
then
  rank='C'
elif [[ $taxlevel == 'phylum' ]]
then
  rank='P'
else
  rank='K'
fi
echo $rank
