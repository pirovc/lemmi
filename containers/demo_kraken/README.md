{- Outdated, instead use the kaiju container with its Snakemake pipeline as demo -}

###### This is the DEMO LEMMI container, using Kraken as example, to illustrate the creation of a container to contribute and appear on the benchmarking platform. Use it as a starting point for creating your own.

See https://gitlab.com/ezlab/lemmi/wikis/userguide#workflow

>>>

The example scenario can be launched and run on a laptop in a few seconds, classifying 1 ecoli reads based on 10 lines of its genomes fasta, using 8-mers and 4 as minimizer.

`./run_test.sh user_uid` # see below

If your tool requires a more complex example to be able to provide an output, you can create your own or contact us.

We run a more complex test scenario on any submitted tool to provide an immediate feedback and ask for debug before running ressource and time consuming benchmarking runs.

>>>

See http://bioboxes.org/docs/ for the original documentation behind the bioboxes creation. 

The LEMMI containers do not have validation steps. They implement two tasks, *build_ref* and *analysis*

Note: the `bbx` folder is provided as example data of what the pipeline is going to use, this is not something you have to submit. However, if you want to use a LEMMI container to conduct your own analyses (build a ref, then analyze your sample), you have to create a similar directory and file structure.

Edit `Dockerfile`, `Taskfile`, add any extra script you need into `Dockerfile`

to test, do `docker build . -t kraken`, and then call `run_test.sh`, see below for detailed commands.

to submit, do `docker build . -t kraken`, then `docker save > kraken.tar`, `gzip kraken.tar` then submit `kraken.tar.gz` to LEMMI (i.e. please contact us to coordinate: ez at ezlab.org). Keep the same name for buidling the container and naming the archives.

Q: why not just submitting the Docker file ? A: The box cannot access root operation or an external connection during benchmark, therefore these steps are made on the submitter's environnement. No download allowed during the run, your tool has to cope with this. Then we suggest to provide the source on this git repository for any tool user to rebuild it.

For a custom reference folder (as example if using the database called minikraken in its dedicated folder), `tar zcfv kraken.reference.tar.gz minikraken` to submit to LEMMI `kraken.reference.tar.gz`.

Will be made available to the box in `/bbx/reference/minikraken` when running the analysis. In this case, the container need to be called with `-e reference=minikraken`. See below for commands.

###### The box Taskfile

- It contains the **build_ref** and **analysis** tasks. If you intend to run only the "GIVEN" mode in the LEMMI benchmark, as kraken would do using minikraken, ignore build_ref.

- When *buil_ref* is run, the task create the db in /bbx/reference/$reference, using the content of /bbx/input/training/ as input. This folder will be made available to the box when running the analysis.

- The `bbx` directory contains an example of the hierachy of directory to be mounted during benchmark by the LEMMI platform, with example of files.

- To test, you just need to replace the empty /bbx/input/training/*.dmp by an up to date version from here: ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz

- the env variable $parameter can pass only one value, but you can use a string that can be split as seen in the Taskfile (parameter=foo,bar). So if you plan to explore parameters on the LEMMI platform, you can pass several parameters here.

- /bbx/input is read only, please work in /bbx/tmp. Disk space is not infinite, do not duplicated fasta if not required. Clean as you go. Use find, not ls => argument list too long. Expect to deal with +100,000 fasta

Once you built your LEMMI container, you can test it step by step as follows (or use run_test.sh, which will clean the bbx folder before each run). Be in the demo_kraken folder when calling these commands (after docker build):

```
u=1001 # the uid of an unpriviledged user that can read/write on $(pwd)/bbx/ (your user id should be ideal for tests => bash command `id` to see it). Don't forget, boxes won't be run with the root user, so all steps need to be completed by an unpriviledged user. Also, no Internet connection, everything has to be there.
```

```
docker run -e thread=1 -e taxlevel=genus -e reference=myref -e parameter=8,4 -v $(pwd)/bbx/input/:/bbx/input:ro -v $(pwd)/bbx/output/:/bbx/output:rw -v $(pwd)/bbx/tmp/:/bbx/tmp:rw -v $(pwd)/bbx/metadata/:/bbx/metadata:rw -v $(pwd)/bbx/reference/:/bbx/reference:rw -u $u kraken build_ref
```

```
docker run -e thread=1 -e taxlevel=genus -e reference=myref -v $(pwd)/bbx/input/:/bbx/input:ro -v $(pwd)/bbx/output/:/bbx/output:rw -v $(pwd)/bbx/tmp/:/bbx/tmp:rw -v $(pwd)/bbx/metadata/:/bbx/metadata:rw -v $(pwd)/bbx/reference/:/bbx/reference:ro -u $u kraken analysis
```

And find the log of the run in `bbx/metadata/`

Results in `bbx/output/`

Note: no need to filter the taxonomical level below the required rank, even though the evaluation will be on the genus level, the rest will be ignored.
