#!/bin/bash
find /bbx/tmp/input/ -name "*.fa" -exec kraken-build --add-to-library {} --db /bbx/reference/$reference/ \;
find /bbx/tmp/input/ -name "*.fa" -exec rm {} \;
