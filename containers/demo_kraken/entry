#!/bin/bash

# exit script if one command fails
set -o errexit

# exit script if Variable is not set
set -o nounset

INPUT=/bbx/input/
OUTPUT=/bbx/output/
TMP_DIR=/bbx/tmp/
METADATA=/bbx/metadata/
REFERENCE=/bbx/reference/

# test that everything was mounted
ls ${INPUT}/ > /dev/null
ls ${OUTPUT} > /dev/null
ls ${TMP_DIR} > /dev/null
ls ${METADATA} > /dev/null
ls ${REFERENCE} > /dev/null

# Since this script is the entrypoint to your container
# you can access the task in `docker run task` as the first argument
TASK=$1

# Use grep to get $TASK in /Taskfile
CMD=$(egrep ^${TASK}: /Taskfile | cut -f 2 -d ':')
if [[ -z ${CMD} ]]; then
  echo "Abort, no task found for '${TASK}'."
  exit 1
fi

# if /bbx/metadata is mounted create log.txt
if [ -d "$METADATA" ]; then
  CMD="($CMD) >& $METADATA/log.txt"
fi

# Run the given task with eval.
# Eval evaluates a String as if you would use it on a command line.
eval ${CMD}
